package usthon.syafii.utsfix

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_kategori.view.*
import kotlinx.android.synthetic.main.frag_data_merk.view.*

class FragmentBrand : Fragment(), View.OnClickListener {

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btninsertmerk ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
            R.id.btndeletemerk ->{
                builder.setTitle("Konfirmasi").setMessage("Yakin ingin menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnDeleteDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
            R.id.btnupdatemerk ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang akan diubah sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnUpdateDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
        }
    }

    lateinit var thisParent: MainActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var v : View
    lateinit var builder : AlertDialog.Builder
    var idmerk : String = ""


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        thisParent = activity as MainActivity
        db = thisParent.getDbObject()

        v = inflater.inflate(R.layout.frag_data_merk, container, false)
        v.btnupdatemerk.setOnClickListener(this)
        v.btninsertmerk.setOnClickListener(this)
        v.btndeletemerk.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)
        v.lsmerk.setOnItemClickListener(itemClick)
        return v
    }
    fun showDataMerk(){
        val cursor : Cursor = db.query("merk", arrayOf("nama_merk as nama","idmerk as _id"),
            null,null,null,null,"nama_merk asc")
        adapter = SimpleCursorAdapter(thisParent,R.layout.item_data_kat,cursor,
            arrayOf("_id","nama"), intArrayOf(R.id.txidkat, R.id.txnamakat),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsmerk.adapter = adapter
    }
    override fun onStart() {
        super.onStart()
        showDataMerk()
    }
    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        idmerk = c.getString(c.getColumnIndex("_id"))
        v.ednamamerk.setText(c.getString(c.getColumnIndex("nama")))
    }

    fun insertDataMerk(namamerk : String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_merk",namamerk)
        db.insert("merk",null,cv)
        showDataMerk()
    }

    fun updateDataMerk(namamerk : String, idmerk : String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_merk",namamerk)
        db.update("merk",cv,"idmerk = $idmerk",null)
        showDataMerk()
    }

    fun deleteDataMerk(idmerk: String){
        db.delete("merk","idmerk = $idmerk", null)
        showDataMerk()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertDataMerk(v.ednamamerk.text.toString())
        v.ednamamerk.setText("")
    }

    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        updateDataMerk(v.ednamamerk.text.toString(),idmerk)
        v.ednamamerk.setText("")
    }

    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataMerk(idmerk)
        v.ednamamerk.setText("")
    }
}