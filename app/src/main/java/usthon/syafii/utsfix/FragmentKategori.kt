package usthon.syafii.utsfix

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import android.widget.ListAdapter
import kotlinx.android.synthetic.main.frag_data_kategori.view.*

class FragmentKategori() : Fragment(),View.OnClickListener {


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btninsertkat ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
            R.id.btndeletekat ->{
                builder.setTitle("Konfirmasi").setMessage("Yakin ingin menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnDeleteDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
            R.id.btnupdatekat ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang akan diubah sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnUpdateDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
        }
    }

    lateinit var thisParent: MainActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var v : View
    lateinit var builder : AlertDialog.Builder
    var idkat : String = ""


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        thisParent = activity as MainActivity
        db = thisParent.getDbObject()

        v = inflater.inflate(R.layout.frag_data_kategori, container, false)
        v.btnupdatekat.setOnClickListener(this)
        v.btninsertkat.setOnClickListener(this)
        v.btndeletekat.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)
        v.lskat.setOnItemClickListener(itemClick)
        return v
    }
    fun showDataKat(){
        val cursor : Cursor = db.query("kategori", arrayOf("nama_kat as nama","idkat as _id"),
            null,null,null,null,"nama_kat asc")
        adapter = SimpleCursorAdapter(thisParent,R.layout.item_data_kat,cursor,
            arrayOf("_id","nama"), intArrayOf(R.id.txidkat, R.id.txnamakat),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lskat.adapter = adapter
    }
    override fun onStart() {
        super.onStart()
        showDataKat()
    }
    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        idkat = c.getString(c.getColumnIndex("_id"))
        v.ednamakat.setText(c.getString(c.getColumnIndex("nama")))
    }

    fun insertDataKat(namakat : String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_kat",namakat)
        db.insert("kategori",null,cv)
        showDataKat()
    }

    fun updateDataKat(namakat : String, idkat : String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_kat",namakat)
        db.update("kategori",cv,"idkat = $idkat",null)
        showDataKat()
    }

    fun deleteDataKat(idkat: String){
        db.delete("kategori","idkat = $idkat", null)
        showDataKat()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertDataKat(v.ednamakat.text.toString())
        v.ednamakat.setText("")
    }

    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        updateDataKat(v.ednamakat.text.toString(),idkat)
        v.ednamakat.setText("")
    }

    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataKat(idkat)
        v.ednamakat.setText("")
    }

}