package usthon.syafii.utsfix

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var db : SQLiteDatabase
    lateinit var fraghb : FragmentHijab
    lateinit var fragkat :FragmentKategori
    lateinit var fragmerk : FragmentBrand
    lateinit var ft : FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fraghb = FragmentHijab()
        fragkat = FragmentKategori()
        fragmerk = FragmentBrand()
        db = DBOpenHelper(this).writableDatabase
    }

    fun getDbObject() : SQLiteDatabase {
        return db
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when(p0.itemId)
        {
            R.id.idkat ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragkat).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.idhijab ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fraghb).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.idmerk ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragmerk).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.idhome ->frameLayout.visibility = View.GONE

        }
        return true
    }
}
