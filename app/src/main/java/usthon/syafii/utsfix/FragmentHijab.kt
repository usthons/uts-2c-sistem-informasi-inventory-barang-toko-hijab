package usthon.syafii.utsfix

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_hijab.*
import kotlinx.android.synthetic.main.frag_data_hijab.view.*
import kotlinx.android.synthetic.main.frag_data_hijab.view.spinner

class FragmentHijab : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener  {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btninserthijab->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnupdatehijab->{

            }
            R.id.btndeletehijab->{

            }
        }
    }

    lateinit var thisParent : MainActivity
    lateinit var v : View
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter : SimpleCursorAdapter
    lateinit var spAdapter1 : SimpleCursorAdapter
    lateinit var dialog : AlertDialog.Builder
    lateinit var db : SQLiteDatabase
    var namaKategori : String = ""
    var namaMerek : String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_hijab,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btninserthijab.setOnClickListener(this)
        v.btnupdatehijab.setOnClickListener(this)
        v.btndeletehijab.setOnClickListener(this)
        v.spinner.onItemSelectedListener = this
        v.spinner2.onItemSelectedListener = this
        return v
    }

    override fun onStart() {
        super.onStart()
        showDataHijab()
        showDataKategori()
        showDataMerek()
    }

    fun showDataHijab(){
        var sql= "select hijab.id as _id, hijab.nama, kategori.nama_kat, merk.nama_merk, hijab.harga, " +
                "hijab.stok from hijab,kategori,merk where hijab.idkat=kategori.idkat and hijab.idmerk=merk.idmerk"
        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_hijab,c,
            arrayOf("_id","nama","nama_kat","nama_merk","harga","stok"), intArrayOf(R.id.txId,R.id.txNama,R.id.txKategori,R.id.txMerk,R.id.txHarga,R.id.txStok),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lshijab.adapter = lsAdapter
    }

    fun showDataKategori(){
        val c : Cursor = db.rawQuery("select nama_kat as _id from kategori order by nama_kat asc", null)
        spAdapter1 = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner.adapter = spAdapter1
        v.spinner.setSelection(0)
    }

    fun showDataMerek(){
        val c1 : Cursor = db.rawQuery("select nama_merk as _id from merk order by nama_merk asc", null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c1,
            arrayOf("_id"), intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner2.adapter = spAdapter
        v.spinner2.setSelection(0)
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner.setSelection(0,true)
        spinner2.setSelection(0,true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c = spAdapter1.getItem(position) as Cursor
        val c1 = spAdapter.getItem(position) as Cursor
        namaKategori = c1.getString(c.getColumnIndex("_id"))
        namaMerek = c.getString(c.getColumnIndex("_id"))
    }

    fun insertDataHijab(id : String,nama : String, idkat : Int, idmerk : Int, harga : String, stok : String){
        var sql = "insert into hijab(id, nama, idkat, idmerk, harga, stok) values (?,?,?,?,?,?)"
        db.execSQL(sql, arrayOf(id,nama,idkat,idmerk,harga,stok))
        showDataHijab()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sqlkat = "select idkat from kategori where nama_kat = '$namaKategori'"
        var sqlmerk = "select idmerk from merk where nama_merk = '$namaMerek'"
        val c : Cursor = db.rawQuery(sqlkat,null)
        val c1 : Cursor = db.rawQuery(sqlmerk,null)
        if(c.count>0 && c1.count>0){
            c.moveToFirst()
            c1.moveToFirst()
            insertDataHijab(
                v.edKode.text.toString(), v.edNama.text.toString(),
                c.getInt(c.getColumnIndex("idkat")),
                c1.getInt(c1.getColumnIndex("idmerk")),
                v.edHarga.text.toString(),
                v.edStok.text.toString()
            )

            v.edKode.setText("")
            v.edNama.setText("")
            v.spinner.setSelection(0)
            v.spinner2.setSelection(0)
            v.edHarga.setText("")
            v.edStok.setText("")
        }
    }

}