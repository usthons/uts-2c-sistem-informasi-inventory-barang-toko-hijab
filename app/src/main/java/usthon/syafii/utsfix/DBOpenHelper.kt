package usthon.syafii.utsfix

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context):SQLiteOpenHelper(context,DB_Name,null,DB_ver) {

    override fun onCreate(db: SQLiteDatabase?) {
        val thb = "create  table hijab (id text primary key, nama text not null, idkat integer not null, idmerk not null, harga text not null, stok text)"
        val tkat = "create table kategori (idkat integer primary key autoincrement not null, nama_kat text not null)"
        val tmerk = "create table merk (idmerk integer primary key autoincrement not null, nama_merk text not null)"
        db?.execSQL(thb)
        db?.execSQL(tkat)
        db?.execSQL(tmerk)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    companion object{
        val DB_Name = "hijab"
        val DB_ver = 1
    }
}